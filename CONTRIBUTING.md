# Contributing

1. File an issue to notify the maintainers about what you're working on.
2. Fork the repo, develop and test your code changes, add docs.
3. Make sure that your commit messages clearly describe the changes.
4. Send a pull request.

NOTE: Always fork the repository and create MR targeting master branch of this repository.

[Staying in sync](#syncing) with master branch of this repository

## Filing an issue
Use the issue tracker to start the discussion. It is possible that someone else is
already working on your idea, your approach is not quite right, or that the functionality
exists already. The ticket you file in the issue tracker will be used to hash that all out.

## Code Style

* Write in UTF-8 encoding
* Always use 4 spaces for indentation (Do **NOT** use tabs!)
* Always try to limit line length to 80 characters
* Do **NOT** commit more than 5 files in one commit
* Always set LF as line separators. Do **NOT** use CRLF
* Look at the existing style and adhere accordingly

## Forking the repository
Be sure to do relevant tests on layouts and styles before making the pull request.
You should also build the docs yourself, add comments on changes you have code and make sure they're readable.

## Making Pull Request
Once you have made all your changes, tests, make a pull request to move everything back into the main branch of the
repository. Be sure to reference the original issue in the pull request.

## Syncing
**Note**: Your fork is the "origin" and the repository you forked from is the "upstream".

* Open an issue on Issue Tracker to specify the feature/fix you are going to develop.
* Login to your GitLab account and fork the repository.

Let's assume that you already cloned your fork to your computer.

Now, Follow these steps

* Add an upstream remote to your cloned fork:

    `git remote add upstream https://gitlab.com/technowolf/arcane.git`
* Fetch commits and branches from upstream remote:

    `git fetch upstream`
* Switch to the master branch of your fork:

    `git checkout master`
* Stash the changes of **your** "master" branch:

    `git stash`
* Merge the changes from the "master" branch of the "upstream" into your the "master" branch of your "origin":

    `git merge upstream/master`
* Resolve merge conflicts if any and commit your merge:

    `git commit -am "Merged from upstream"`
* Push the changes to your fork:

    `git push`
* Get back your stashed changes:

    `git stash pop`

You're done! Congratulations!

**Good Luck!**
