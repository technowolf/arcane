## V 1.0

### Buttons   
    * Basic Buttons
    * Animated Buttons
    * 3D buttons
    * Square, Round, Pills and Full Width Buttons
        (Minified version available)
    
### Table
    * Basic table
    * Striped table
    * Boarded table
    
### Social buttons
    * Facebook
    * Twitter
    * Google Plus
    * Dribble
    * LinkedIn
    * Instagram
    * Pinterest
    * Github
    * GitLab
    * Flickr
    * VK

### Link Animation
    * 5 link animation styles